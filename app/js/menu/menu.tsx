import * as MagicPainting from "../../../src/index.js";

const transformations = MagicPainting.transformations;

const i2trsfs = [
	0,
	transformations.SYMMETRY_X,
	transformations.SYMMETRY_Y,
	transformations.SYMMETRY_X | transformations.SYMMETRY_Y | transformations.ROTATION_180,
	transformations.ROTATION_60 | transformations.ROTATION_120 | transformations.ROTATION_180 | transformations.ROTATION_240 | transformations.ROTATION_300,
];

export type EffectSettingsProps<T extends PlainObject = {}> = PlainObject & {
	currentSettings?: T;
	setSettings: (settings: T) => void;
};
type Effect<T extends PlainObject = {}> = PlainObject & {
	Effect: typeof MagicPainting.Effect;
	Icon: string | React.FC;
	Settings: React.FC<EffectSettingsProps<T>>;
};

//======================================================================
// S V G
//======================================================================
export const SVGIcon = (props: PlainObject) => {
	const attrs = {
		viewBox: `0 0 ${props.width ?? 10} 10`,
		className: "icon",
	};
	const color = props.color;
	const children = React.Children.map(props.children, child => React.cloneElement(child, { color }));
	return (<svg {...attrs}>{children}</svg>);
};

export function object2svgAttrs(o: PlainObject, props: PlainObject = {}) {
	const ret: PlainObject = {};
	//
	const v = o.stroke;
	if (v) {
		if (v === true) ret.stroke = props.color;
		if (typeof v === "string") ret.stroke = v;
		else if (typeof v === "object") {
			ret.stroke = v.color ?? o.color;
			if (v.width) ret["stroke-width"] = v.width;
			if (v.linejoin) ret["stroke-linejoin"] = v.linejoin;
			if (v.dasharray) ret["stroke-dasharray"] = v.dasharray;
		}
	} else {
		ret.stroke = "none";
	}
	if (!o.fill) ret.fill = "none";
	if (o.transform) ret.transform = o.transform;
	// TODO
	//
	return ret;
}
export const SVGLine = (props: PlainObject) => {
	const { p1, p2, ...rest } = props;
	const { x: x1, y: y1 } = p1;
	const { x: x2, y: y2 } = p2;
	const attrs = { x1, y1, x2, y2, ...object2svgAttrs(rest, props) };
	return (<line {...attrs} />);
};
export const SVGPath = (props: PlainObject) => {
	const { d, ...rest } = props;
	const attrs = { d, ...object2svgAttrs(rest, props) };
	return (<path {...attrs} />);
};

const SVGLShape = (props: PlainObject) => (<SVGPath d="M0,0v3.5h2.5v-1.5h-1v-2z" fill={true} {...props} />);
const SVGIconSymmetry1 = (props: PlainObject) => (<SVGIcon {...props}>
	<SVGLine p1={{ x: 0, y: 5 }} p2={{ x: 10, y: 5 }} stroke={{ width: .5 }} />
	<SVGLShape transform="translate(4,0)" />
	<SVGLShape transform="translate(4,10) scale(1,-1)" />
</SVGIcon>);
const SVGIconSymmetry2 = (props: PlainObject) => (<SVGIcon {...props}>
	<SVGLine p1={{ x: 5, y: 0 }} p2={{ x: 5, y: 10 }} stroke={{ width: .5 }} />
	<SVGLShape transform="translate(0,4)" />
	<SVGLShape transform="translate(10,4) scale(-1,1)" />
</SVGIcon>);
const SVGIconSymmetry3 = (props: PlainObject) => (<SVGIcon {...props}>
	<SVGPath d="M0,5h10M5,0v10" stroke={{ width: .5 }} />
	<SVGLShape transform="" />
	<SVGLShape transform="translate(10,0) scale(-1,1)" />
	<SVGLShape transform="translate(0,10) scale(1,-1)" />
	<SVGLShape transform="translate(10,10) scale(-1,-1)" />
</SVGIcon>);
const SVGIconSymmetry4 = (props: PlainObject) => (<SVGIcon {...props}>
	<SVGLShape transform="                translate(2,0)" />
	<SVGLShape transform="rotate(60,5,5)  translate(2,0)" />
	<SVGLShape transform="rotate(120,5,5) translate(2,0)" />
	<SVGLShape transform="rotate(180,5,5) translate(2,0)" />
	<SVGLShape transform="rotate(240,5,5) translate(2,0)" />
	<SVGLShape transform="rotate(300,5,5) translate(2,0)" />
</SVGIcon>);

const SVGIconSymmetry = (props: PlainObject) => {
	switch (props.type) {
		case 1: return (<SVGIconSymmetry1 />);
		case 2: return (<SVGIconSymmetry2 />);
		case 3: return (<SVGIconSymmetry3 />);
		case 4: return (<SVGIconSymmetry4 />);
		default: return (<SVGIcon />);
	}
};
const SVGSymmetryIcon = (props: PlainObject) => (<React.Fragment>
	<SVGLine p1={{ x: 8, y: 0 }} p2={{ x: 8, y: 10 }} stroke={{ width: 1, dasharray: "2 2" }} />
	<SVGPath d="M6,.5L.5,9.5H6zM10,.5L15.5,9.6H10z" stroke={{ width: 1, linejoin: "round" }} fill={true} />
</React.Fragment>);

//======================================================================
// B U T T O N S
//======================================================================
export const Button = (props: PlainObject) => {
	props.variant ??= "outline-dark";
	return (<ReactBootstrap.Button {...props} />);
};

export const SVGIconButton = (props: PlainObject) => (<Button onClick={props.onClick}>
	<div className="fa">
		<SVGIcon {...props} />
	</div>
</Button>);

export const FontAwesomeButton = (props: PlainObject) => {
	const { icon, ...attrs } = props;
	const cls = `fa fa-${icon}`;
	return (<Button {...attrs}>
		<div className={cls} />
	</Button>);
};

//======================================================================
// M O D A L S
//======================================================================
type MenuClearProps = {
	clear: Function,
};
const MenuClear = (props: MenuClearProps) => {
	const [show, setShow] = React.useState(false);
	const handleHide = () => setShow(false);
	const Modal = ReactBootstrap.Modal;
	return (<React.Fragment>
		<FontAwesomeButton icon="trash" onClick={() => setShow(true)} />
		<Modal centered show={show} onHide={handleHide}>
			<Modal.Header>
				<Modal.Title>
					Clear the painting?
				</Modal.Title>
			</Modal.Header>
			<Modal.Footer>
				<Button onClick={handleHide}>cancel</Button>
				<Button variant="outline-danger" onClick={() => {
					handleHide();
					props.clear();
				}}>
					clear <div className="fa fa-trash"></div>
				</Button>
			</Modal.Footer>
		</Modal>
	</React.Fragment>);
};

type MenuSymmetryProps = {
	onClick: Function,
	type: number,
};
const MenuSymmetry = (props: MenuSymmetryProps) => {
	const [show, setShow] = React.useState(false);
	const handleClick = (i: number) => {
		setShow(false);
		props.onClick(i);
	};
	return (<div>
		<SVGIconButton onClick={() => setShow(true)} width={16}>
			<SVGSymmetryIcon />
		</SVGIconButton>
		<div>
			<SVGIconSymmetry type={props.type} />
		</div>
		<ReactBootstrap.Modal centered show={show} onHide={() => setShow(false)}>
			<ReactBootstrap.ButtonGroup vertical>
				<Button onClick={() => handleClick(0)}>
					none
				</Button>
				<Button onClick={() => handleClick(1)}>
					<SVGIconSymmetry1 />
				</Button>
				<Button onClick={() => handleClick(2)}>
					<SVGIconSymmetry2 />
				</Button>
				<Button onClick={() => handleClick(3)}>
					<SVGIconSymmetry3 />
				</Button>
				<Button onClick={() => handleClick(4)}>
					<SVGIconSymmetry4 />
				</Button>
			</ReactBootstrap.ButtonGroup>
		</ReactBootstrap.Modal>
	</div>);
};

type MenuEffectProps = PlainObject & {
	effects: { [x: string]: Effect; },
};
const MenuEffect = (props: MenuEffectProps) => {
	const [show, setShow] = React.useState(false);
	const handleClick = (k: string) => {
		setShow(false);
		props.onClick(k);
	};
	const buttonEffects = Object.entries(props.effects).map(([k, v]) => {
		return (<Button onClick={() => handleClick(k)}>
			<div className="effect">
				<div>{k}</div>
				<div className="effect-icon">
					<v.Icon />
				</div>
			</div>
		</Button>);
	});
	return (<div>
		<FontAwesomeButton icon="bars" onClick={() => setShow(true)} />
		<div className="effect-icon">
			<props.effect.Icon />
		</div>
		<ReactBootstrap.Modal centered show={show} onHide={() => setShow(false)}>
			<ReactBootstrap.ButtonGroup vertical>
				{buttonEffects}
			</ReactBootstrap.ButtonGroup>
		</ReactBootstrap.Modal>
	</div>);
};

type MenuSettingsProps = {
	currentEffect: Effect,
	currentSettings: { [x: string]: PlainObject; },
	setSettings: (settings: PlainObject) => void,
	submitSettings: () => void,
	effect: string,
};
const MenuSettings = (props: MenuSettingsProps) => {
	const [show, setShow] = React.useState(false);
	const handleShow = () => setShow(true);
	const handleHide = () => setShow(false);
	const handleClick = () => {
		handleHide();
		props.submitSettings();
	};
	const Modal = ReactBootstrap.Modal;
	const { currentEffect, currentSettings, setSettings } = props;
	const { Icon, Settings } = currentEffect;
	return (<React.Fragment>
		<FontAwesomeButton icon="tools" onClick={handleShow} />
		<Modal centered backdrop="static" keyboard={false} show={show}>
			<Modal.Header>
				Settings of {props.effect}
				<Icon />
			</Modal.Header>
			<Modal.Body>
				<Settings {...{ currentSettings, setSettings }} />
			</Modal.Body>
			<Modal.Footer>
				<Button variant="outline-success" onClick={handleClick}>
					OK
				</Button>
			</Modal.Footer>
		</Modal>
	</React.Fragment>);
};

type MenuInfoProps = {
	portrait: boolean,
};
const MenuInfo = (props: MenuInfoProps) => {
	const [show, setShow] = React.useState(false);
	const OC = ReactBootstrap.Offcanvas;
	const placement = props.portrait ? "bottom" : "start";
	return (<React.Fragment>
		<FontAwesomeButton icon="question" onClick={() => setShow(true)} />
		<OC show={show} placement={placement} onHide={() => setShow(false)} className="info">
			<OC.Header closeButton>
				<OC.Title>
					<div>
						Magic Painting
					</div>
					<div className="subtitle">
						easy interactive beautiful artistic abstract panting
					</div>
				</OC.Title>
			</OC.Header>
			<OC.Body>
				<div>
					<span className="li">
						<span className="fa fa-trash icon"></span>
					</span>
					<span>
						clear painting
					</span>
				</div>
				<div>
					<span className="li">
						<span className="fa fa-file-download icon"></span>
					</span>
					<span>
						download as image file
					</span>
				</div>
				<div>
					<span className="li">
						<span className="fa fa-reply icon"></span>
						<span className="fa fa-share icon"></span>
					</span>
					<span>
						undo / redo
					</span>
				</div>
				<div>
					<span className="li">
						<div className="fa">
							<SVGIcon width={16}>
								<SVGSymmetryIcon />
							</SVGIcon>
						</div>
					</span>
					<span>
						transformation(s) selection
					</span>
				</div>
				<div>
					<span className="li">
						<span className="fa fa-images icon"></span>
					</span>
					<span>
						see gallery
					</span>
				</div>
				<div>
					<span className="li">
						<span className="fa fa-expand icon"></span>
					</span>
					<span>
						fullscreen mode
					</span>
				</div>
				<div>
					<span className="li">
						<span className="fa fa-tools icon"></span>
					</span>
					<span>
						effect settings
					</span>
				</div>
				<div>
					<span className="li">
						<span className="fa fa-bars icon"></span>
					</span>
					<span>
						effect selection
					</span>
				</div>
				<div className="footer">
					<div>
						<a href="https://docs.google.com/forms/d/e/1FAIpQLSfTD9S5lIrzzfUGcJ2JEer4Ma5-QbF_7wpDgmoGE-fIUz_kaQ/viewform?usp=pp_url">
							<span className="fa fa-envelope icon"></span>
							contact maintainer
						</a>
					</div>
					<div>
						<a href="https://gitlab.com/stranskyjan/magic-painting">
							<span className="fab fa-gitlab icon"></span>
							source code
						</a>
					</div>
				</div>
			</OC.Body>
		</OC>
	</React.Fragment>);
};

//======================================================================
// M A I N
//======================================================================
type MenuMainProps = {
	magicPainting: MagicPainting.MagicPainting,
	clear: Function,
	save: Function,
	undo: Function,
	redo: Function,
	setTransformations: Function,
	effect: string,
	effects: { [x: string]: Effect; },
	setEffect: Function,
	currentSettings: { [x: string]: PlainObject; },
	setSettings: (settings: PlainObject) => void,
	submitSettings: () => void,
	transformations: number,
	portrait: boolean,
};
const MenuMain = (props: MenuMainProps) => {
	const {
		magicPainting,
		clear, save, undo, redo,
		setTransformations,
		effect, effects, setEffect,
		currentSettings, setSettings, submitSettings,
		portrait,
	} = props;
	//
	const [, setStateForceUpdate] = React.useState(1);
	const forceUpdate = () => setStateForceUpdate(i => i + 1);
	magicPainting.onHistoryChange(forceUpdate);
	//
	const currentEffect = effects[effect];
	const gallery = () => {
		const a = document.createElement("a");
		a.href = "gallery.html";
		a.click();
	};
	return (<div className="main">
		<div>
			<MenuClear clear={clear} />
			<FontAwesomeButton icon="file-download" onClick={save} />
		</div>
		<div>
			<FontAwesomeButton icon="reply" disabled={!magicPainting.isUndoable} onClick={undo} />
			<FontAwesomeButton icon="share" disabled={!magicPainting.isRedoable} onClick={redo} />
		</div>
		<MenuSymmetry type={props.transformations} onClick={setTransformations} />
		<div>
			<MenuInfo portrait={portrait} />
			<FontAwesomeButton icon="images" onClick={gallery} />
		</div>
		<div>
			<MenuSettings {...{ currentEffect, effect, currentSettings, setSettings, submitSettings }} />
			<FontAwesomeButton icon="expand" onClick={() => document.body.requestFullscreen()} />
		</div>
		<MenuEffect effect={currentEffect} effects={effects} onClick={setEffect} />
	</div>);
};

//======================================================================
// M E N U
//======================================================================
type MenuProps = {
	effects?: { [x: string]: Effect; };
	magicPainting: MagicPainting.MagicPainting;
};
type MenuState = {
	transformations: number,
	effect: string,
	settings: PlainObject,
};
export function Menu(props: MenuProps) {
	const [state, setState] = React.useState<MenuState>({
		transformations: 3,
		effect: Object.keys(props.effects)[0],
		settings: {},
	});
	//
	React.useEffect(() => {
		setEffect();
		setSettings();
		setTransformations();
	}, []);
	//
	const setEffect = (effect?: string) => {
		if (effect === undefined) {
			setEffectCallback();
			return;
		}
		setState(state => ({ ...state, effect }));
	};
	const setEffectCallback = () => {
		var E = props.effects[state.effect].Effect;
		props.magicPainting.effect = new E();
		setSettingsCallback();
	};
	React.useEffect(setEffectCallback, [state.effect]);
	//
	const setSettings = (esettings: PlainObject = {}) => {
		var settings = { ...state.settings };
		settings[state.effect] = esettings;
		setState(state => ({ ...state, settings }));
	};
	const setSettingsCallback = () => {
		props.magicPainting.settings = state.settings[state.effect];
	};
	const submitSettings = () => {
		props.magicPainting.submitSettings();
	};
	React.useEffect(setSettingsCallback, [JSON.stringify(state.settings)]);
	//
	const setTransformations = (transformations?: number) => {
		if (transformations === undefined) {
			trsfCallback();
			return;
		}
		setState(state => ({ ...state, transformations }));
	};
	const trsfCallback = () => {
		props.magicPainting.transformations = i2trsfs[state.transformations];
	};
	React.useEffect(trsfCallback, [state.transformations]);
	//
	const [, setForceUpdateState] = React.useState(1);
	const forceUpdate = () => setForceUpdateState(i => i + 1);
	//
	const magicPainting = props.magicPainting;
	const clear = () => magicPainting.clear();
	const save = () => magicPainting.save();
	const undo = () => { magicPainting.undo(); forceUpdate(); };
	const redo = () => { magicPainting.redo(); forceUpdate(); };
	const { transformations, effect, settings } = state;
	const { effects } = props;
	const currentSettings = { ...(settings[effect] ?? {}) };
	const portrait = window.innerHeight > window.innerWidth;
	const menuMainProps = {
		magicPainting,
		clear, save, undo, redo,
		transformations, setTransformations,
		currentSettings, setSettings, submitSettings,
		effect, effects, setEffect,
		portrait,
	};
	//
	return (<MenuMain {...menuMainProps} />);
}
