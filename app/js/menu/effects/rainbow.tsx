import * as MagicPainting from "../../../../src/index.js";
import type { EffectSettingsProps } from "../menu";

const Effect = MagicPainting.effects.Rainbow;

const Icon = () => <img alt="R" src="images/effect-icons/rainbow.png"></img>;

type Props = EffectSettingsProps<{}>;
function Settings(props: Props) {
	return <div>no settings</div>;
}

export default { Icon, Settings, Effect };
