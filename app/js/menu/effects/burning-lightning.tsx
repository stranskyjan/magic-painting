import * as MagicPainting from "../../../../src/index.js";
import type { EffectSettingsProps } from "../menu";

const Effect = MagicPainting.effects.BurningLightning;

const { Row, Col, Form } = ReactBootstrap;

const Icon = () => <img alt="B" src="images/effect-icons/burning-lightning.png"></img>;

type Props = EffectSettingsProps<{
	color?: string,
}>;
function Settings(props: Props) {
	var { color } = {
		color: "#ff8000",
		...props.currentSettings
	};
	return (<Form>
		<Row>
			<Form.Label>
				line color
			</Form.Label>
			<Col>
				<Form.Control
					type="color"
					title="color"
					value={color}
					onChange={e => props.setSettings({
						...props.currentSettings,
						color: e.target.value,
					})}
				/>
			</Col>
		</Row>
	</Form>);
}

export default { Icon, Settings, Effect };
