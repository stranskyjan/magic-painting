import * as MagicPainting from "../../../../src/index.js";
import type { EffectSettingsProps } from "../menu";

const Effect = MagicPainting.effects.StreamLines;

const Icon = () => <img alt="S" src="images/effect-icons/streamlines.png"></img>;

type Props = EffectSettingsProps<{}>;
function Settings(props: Props) {
	return <div>no settings</div>;
}

export default { Icon, Settings, Effect };
