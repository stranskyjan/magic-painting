import * as MagicPainting from "../../src/index.js";
import * as Menu from "../../app/js/menu/index.js";
window.addEventListener("load", () => {
	var elementMagicPainting = document.getElementById("magic-painting");
	var magicPainting = new MagicPainting.MagicPainting(elementMagicPainting);
	var saved = true;
	magicPainting.onPointerStart(() => saved = false);
	magicPainting.onSave(() => saved = true);
	//
	window.addEventListener("beforeunload", e => {
		var ret = !saved;
		if (ret) e.returnValue = true;
		return ret;
	});
	//
	var elementMenu = document.getElementById("menu");
	var attrs = {
		magicPainting,
		effects: Object.assign({}, Menu.effects),
	};
	ReactDOM.render(React.createElement(Menu.Menu, attrs), elementMenu);
	//
	// https://stackoverflow.com/a/43321596/2700104
	document.addEventListener("mousedown", event => {
		if (event.detail > 1) event.preventDefault();
	}, false);
});
