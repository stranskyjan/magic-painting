import { MagicPainting, Effect, effects, transformations as trsfs } from "../../src/index.js";

type Events = Event[];
type Event = {
	type: "S" | "M" | "E" | "settings";
	t: number;
	x?: number;
	y?: number;
	settings?: PlainObject;
};
type Pieces = Piece[];
type Piece = {
	Effect: typeof Effect;
	title: string;
	events: Events;
	callback?: (mp: MagicPainting) => any;
};

const S = "S";
const M = "M";
const E = "E";
const SETTINGS = "settings";

type number2number = (t: number) => number;
type TimeFunctionParams = {
	fx: number2number;
	fy: number2number;
	ft: number2number;
	start?: "S" | "M";
	n?: 500;
};
function timeFunction(params: TimeFunctionParams) {
	var n = params.n ?? 500;
	var ret = [];
	var { fx, fy, ft } = params;
	for (var i = 0; i < n; i++) {
		var p = i / (n - 1);
		var [x, y, t] = [fx, fy, ft].map(f => f(p));
		var e: Event;
		if (i === 0) e = { type: params.start ?? S, x, y, t };
		else if (i === n - 1) e = { type: E, t };
		else e = { type: M, x, y, t };
		ret.push(e);
	}
	return ret;
}

const T1 = trsfs.SYMMETRY_X;
const T2 = trsfs.SYMMETRY_Y;
const T3 = trsfs.SYMMETRY_X | trsfs.SYMMETRY_Y | trsfs.ROTATION_180;

const pieceSun: Piece = {
	title: "Sun",
	Effect: effects.BurningLightning,
	events: [
		{ type: S, x: 500, y: 500, t: 0 },
		{ type: E, t: 20000 },
	]
};
const pieceDandelion: Piece = {
	title: "Dandelion",
	Effect: effects.StreamLines,
	events: [
		{ type: S, x: 500, y: 250, t: 0 },
		...timeFunction({
			fx: t => 500,
			fy: t => 250 + 600 * t,
			ft: t => 1600 + 100 * t,
			start: M,
		})
	]
};
const pieceInfinity: Piece = {
	title: "Infinity",
	Effect: effects.BurningLightning,
	callback: mp => mp.transformations = T3,
	events: [
		{ type: SETTINGS, t: 0, settings: { color: "#0000ff" } },
		...new Array(8).fill(0).map((_, i) => {
			var even = i % 2 === 0;
			return timeFunction({
				fx: t => 100 + 400 * (even ? t : 1 - t),
				fy: t => 500 + 500 * Math.sqrt(even ? t : 1 - t) * (even ? 1 - t : t),
				ft: t => i * 2000 + 2000 * Math.sqrt(t),
			});
		}).flat(),
	]
};
const pieceCommet: Piece = {
	title: "Commet",
	Effect: effects.StreamLines,
	events: [
		{ type: S, x: 850, y: 150, t: 0 },
		...timeFunction({
			fx: t => 850 - 500 * t,
			fy: t => 150 + 500 * t,
			ft: t => 200 + 400 * t,
			start: M,
		})
	]
};
const piecePrioriIncantatem: Piece = {
	title: "Priori Incantatem",
	Effect: effects.BurningLightning,
	callback: mp => mp.transformations = T1,
	events: [
		{ type: SETTINGS, t: 0, settings: { color: "#22ff33" } },
		...timeFunction({
			fx: t => Math.min(500, 0 + 600 * t),
			fy: t => 500,
			ft: t => 10000 * t,
		}),
		{ type: SETTINGS, t: 0, settings: { color: "#ff2222" } },
		...timeFunction({
			fx: t => Math.max(500, 1000 - 600 * t),
			fy: t => 500,
			ft: t => 10000 + 10000 * t,
		}),
	]
};
const pieceDementor: Piece = {
	title: "Dementor",
	Effect: effects.StreamLines,
	events: timeFunction({
		fx: t => 750 - 500 * Math.sin(t * Math.PI),
		fy: t => 500 - 300 * Math.cos(t * Math.PI),
		ft: t => 6000 * t
	})
};

const pieces: Pieces = [
	pieceSun,
	pieceCommet,
	pieceInfinity,
	pieceDandelion,
	piecePrioriIncantatem,
	pieceDementor,
];

const Gallery = (props: PlainObject) => (<React.Fragment>
	{props.children}
</React.Fragment>);

function animate(magicPainting: MagicPainting, events: Events) {
	function finish() {
		var t = 10000;
		setTimeout(() => magicPainting.clear(), t - 500);
		setTimeout(() => animate(magicPainting, events), t);
	}
	var effect = magicPainting.effect;
	var index = 0;
	var aTime = 0;
	requestAnimationFrame(t => aTime = t);
	function rAF(t: number) {
		while (true) {
			if (index >= events.length) {
				finish();
				return;
			}
			var e = events[index];
			if (e.t + aTime > t) break;
			index++;
			var { type, x, y, settings } = e;
			if (type === S) effect.pointerStart(x, y, t);
			if (type === M) effect.pointerMove(x, y, t);
			if (type === E) effect.pointerEnd(t);
			if (type === SETTINGS) effect.settings = settings;
		}
		requestAnimationFrame(rAF);
	}
	requestAnimationFrame(rAF);
}

type PieceProps = Piece;
function PieceComponent(props: PieceProps) {
	const refMain = React.useRef<HTMLDivElement>();
	const refMagicPainting = React.useRef<HTMLDivElement>();
	//
	React.useEffect(() => {
		var m = refMain.current;
		var container = m.querySelector(".magic-painting-container");
		var mp = refMagicPainting.current;
		var magicPainting = new MagicPainting(mp);
		var effect = magicPainting.effect = new props.Effect();
		props.callback?.(magicPainting);
		magicPainting.freeze();
		m.onclick = () => container.requestFullscreen();
		animate(magicPainting, props.events);
	}, []);
	//
	var children = [
		(<div className="title">{props.title}</div>),
		(<div className="magic-painting-container">
			<div ref={refMagicPainting} className="magic-painting"></div>
		</div>),
	];
	children.push((<div className="effect">{props.Effect.name}</div>));
	//
	return (<div className="piece" ref={refMain}>{children}</div>);
}

window.addEventListener("load", () => {
	var elementGallery = document.getElementById("gallery");
	var pieceComponents = pieces.map((piece) => (<PieceComponent {...piece} />));
	ReactDOM.render((<Gallery>{pieceComponents}</Gallery>), elementGallery);
});

export default undefined;
