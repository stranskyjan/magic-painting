#!/usr/bin/env python3
import sys
import os
import json
from datetime import date

PRE = "--pre" in sys.argv
PLACEHOLDER = "@@VERSION@@"

files = [
	"README.md",
	"examples/web-minimal/cdn.html",
]

if PRE:
	with open("package.json") as f:
		data = json.load(f)
	version = data["version"]
else:
	version = os.environ["npm_package_version"]

l2l = lambda l: l.replace(version,PLACEHOLDER) if PRE else l.replace(PLACEHOLDER,version)

for fn in files:
	with open(fn) as f:
		lines = f.readlines()
	lines = [l2l(l) for l in lines]
	with open(fn,"w") as f:
		f.writelines(lines)

if not PRE:
	unreleased = "## [Unreleased] - yyyy-mm-dd"
	released = unreleased.replace("Unreleased",version).replace("yyyy-mm-dd",date.today().strftime("%Y-%m-%d"))
	with open("CHANGELOG.md") as f:
		lines = f.readlines()
	lines = [l.replace(unreleased,f"{unreleased}\n\n{released}") for l in lines]
	with open("CHANGELOG.md","w") as f:
		f.writelines(lines)

