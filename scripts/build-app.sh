#!/bin/sh
for d in index.html gallery.html css images favicon.svg; do cp -r app/$d dist/app; done
cp demo.gif dist
for f in dist/app/js/app.js dist/app/js/menu/menu.js dist/app/js/gallery.js `ls dist/app/js/menu/effects/*.js`; do
	sed -i 's@/src/@/esm/@g' $f
done
for f in app gallery; do
	node_modules/.bin/rollup dist/app/js/$f.js --file dist/app/$f.js --format iife
	node_modules/.bin/uglifyjs -c -o dist/app/$f.min.js dist/app/$f.js
done
rm -r dist/app/js/*
for f in app gallery; do
	rm dist/app/$f.js
	mv dist/app/$f.min.js dist/app/js
done
for f in index gallery; do
	f="dist/app/$f.html"
	sed -i 's/ type="module"//' $f
	sed -i 's@../out/app/js/\(.\+\).js@js/\1.min.js@' $f
	sed -i '/node_modules/d' $f
	#
	sed -i '3 a <script crossorigin src="https://cdn.jsdelivr.net/npm/tinycolor2@1.4.2/tinycolor.min.js"></script>' $f
	sed -i '3 a <script crossorigin src="https://unpkg.com/react-bootstrap@next/dist/react-bootstrap.min.js"></script>' $f
	sed -i '3 a <script crossorigin src="https://unpkg.com/react-dom@17/umd/react-dom.production.min.js"></script>' $f
	sed -i '3 a <script crossorigin src="https://unpkg.com/react@17/umd/react.production.min.js"></script>' $f
	sed -i '3 a <link rel=stylesheet href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">' $f
	sed -i '3 a <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css">' $f
done

cp dist/index.min.js dist/app/js/magic-painting.min.js
mkdir -p dist/app/examples
cp -r examples/extending/ dist/app/examples
sed -i 's@../../dist/index.min.js@../../js/magic-painting.min.js@' dist/app/examples/extending/index.html
