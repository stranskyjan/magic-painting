#!/bin/sh
echo "- run 'npm start'"
echo "- open 'http://localhost:8765/$1'"
echo "- test each effect"
echo "- confirm the result"
read -p "Tests OK (y/n)? " choice
case "$choice" in
	y|Y)
		exit 0
		;;
	*)
		exit 1
		;;
esac
