// example primary effect
class ExampleEffect extends MagicPainting.Effect {
	constructor() {
		super();
		// what to do on pointer start
		this.onPointerStart((x, y, t) => {
			// stores the coordinates
			this.xy = [{ x, y }];
			// add new effect (which will be started just after)
			this.add(new ExampleStartEffect());
		});
		// what to do on pointer move
		this.onPointerMove((x, y, t) => {
			// updates coordinates
			var { x: x0, y: y0 } = this.xy;
			this.xy = { x, y };
			var dx = x - x0;
			var dy = y - y0;
			var context = this.background;
			// draw line from previous to current coordinates
			context.stroke(() => {
				context.moveTo(x0, y0);
				context.lineTo(x, y);
				context.context.lineWidth = 10;
				context.context.strokeStyle = "cyan";
			});
			// draw perpendicular lines
			context.stroke(() => {
				context.moveTo(x, y);
				context.lineTo(x + dy, y - dx);
				context.context.lineWidth = 5;
				context.context.strokeStyle = "white";
			});
		});
		// what to do on pointer end
		this.onPointerEnd(t => {
			// finish is important for undo / redo
			this.finish();
		});
	}
}
// MagicPainting.effect must be a primary effect. The easiest is
ExampleEffect.bePrimary();

// example secondary effect
class ExampleStartEffect extends MagicPainting.Effect {
	constructor() {
		super();
		// what to do on pointer start
		this.onPointerStart((x, y, t) => {
			// stores coordinates and time
			this.xy = { x, y };
			this.t = t;
		});
		// what to do on animation frame
		this.onAnimationFrameEnd(t => {
			// draw on foreground
			this.draw(this.foreground, t);
		});
		// what to do on pointer end
		this.onPointerEnd(t => {
			// draw on background
			this.draw(this.background, t);
			// remove itself from its parent
			this.remove();
		});
	}
	// what and how to draw (a circle whose radius depends on time elapsed from pointer start)
	draw(context, time) {
		var { x, y } = this.xy;
		var r = 10 * (1 + Math.cos((time - this.t) / 300));
		context.fill(() => {
			context.arc(x, y, r, 0, 2 * Math.PI);
			context.context.fillStyle = "red";
		});
	}
}
