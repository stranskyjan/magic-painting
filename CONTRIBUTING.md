# Contributing

For communication, please use
[GitLab pages](https://gitlab.com/stranskyjan/magic-painting)
(namely [issues](https://gitlab.com/stranskyjan/magic-painting/issues))
or
[e-mail](https://docs.google.com/forms/d/e/1FAIpQLSfTD9S5lIrzzfUGcJ2JEer4Ma5-QbF_7wpDgmoGE-fIUz_kaQ/viewform?usp=pp_url)

## Non-code
-Ideas / feedback about anything concerning the project:
- UI / UX
- code architecture and design
- existing effects
- new effects
- ...

## Code
When contributing to [this repository](https://gitlab.com/stranskyjan/magic-painting), please first discuss the change you wish to make with the maintainer(s) of the repository before making a change.

Please read and follow the following guidelines before code contribution.

### New Effect
See existing effects for inspiration.

To create a new effect, please follow next steps.

- Effect itself:
	- create separate `src/effects/your-effect.ts` file
	- add `export * from "your-effect.js"` to `src/effects/index.ts` file
- In app:
	- add an icon to `app/images/effect-icons` directory
	- add at least one example to `app/js/gallery.tsx` file
	- create separate `app/js/menu/effects/your-effect.tsx` file, being basically:
		```typescript
		import * as MagicPainting from "../../../../src/index.js"
		import {EffectSettings} from "../menu.js"
		const Effect = MagicPainting.effects.YourEffect
		const Icon = ... // icon component
		class Settings extends EffectSettings<Props,State> {
			...
		}
		export default {Icon,Settings,Effect}
		```
	- add `export {default as YourEffect} from "your-effect.js"` to `app/js/menu/effects/index.ts` file
