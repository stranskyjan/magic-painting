import { Effect, Context } from "../magic-painting.js";

const lighter = "lighter";

function last<T>(array: T[]) {
	return array[array.length - 1];
}

const random = (mean: number, dev = 0) => {
	return mean + dev * 2 * (Math.random() - .5);
};

const dtLightning = () => 40;
const randomDuration = () => random(150, 100);
const rdt = () => random(20, 15);
const rdd = () => random(15, 10);
const rda = () => random(0, 1);

export class BurningLightning extends Effect {
	private _xy: XY;
	private _colors: string[];
	private _t: number;
	private _started = false;
	constructor() {
		super();
		this.settings = {
			color: "#ff8000",
		};
		//
		this.onPointerStart((x, y, t) => {
			this._setColors();
			this._xy = { x, y };
			this._started = true;
			this._t = t;
			this.add(new Lightning(x, y, t, this._colors));
		});
		this.onPointerMove((x, y, t) => {
			this._createLightningsUntilTime(t);
			//
			var { x: x0, y: y0 } = this._xy;
			this._xy = { x, y };
			var widths = [5, 3, 1];
			var alphas = [.2, .2, .2];
			var context = this.background;
			for (var i = 0; i < 3; i++) {
				context.stroke(() => {
					context.moveTo(x0, y0);
					context.lineTo(x, y);
					var ctx = context.context;
					ctx.strokeStyle = this._colors[i];
					ctx.lineWidth = widths[i];
					ctx.globalAlpha = alphas[i];
					ctx.globalCompositeOperation = lighter;
				});
			}
		});
		this.onPointerEnd(t => {
			this._createLightningsUntilTime(t);
			this._started = false;
		});
		this.onAnimationFrameEnd(t => {
			if (!this._started) return;
			this._createLightningsUntilTime(t);
		});
		this.onRemoveChild(() => {
			if (this.numberOfChildren === 0) this.finish();
		});
	}
	private _setColors() {
		var c = tinycolor(this.settings.color);
		this._colors = [0, 30, 100].map(v => c.brighten(v).toHexString());
	}
	private _createLightningsUntilTime(t: number) {
		var { x, y } = this._xy;
		while (true) {
			var dt = dtLightning();
			if (this._t + dt > t) break;
			this._t += dt;
			this.add(new Lightning(x, y, this._t, this._colors));
		}
	}
}
BurningLightning.bePrimary();

class Lightning extends Effect {
	private _coords: XY[];
	private _t;
	private _t0;
	private _duration;
	private _angle;
	private _colors;
	constructor(x: number, y: number, t: number, colors: string[]) {
		super();
		this._duration = randomDuration();
		this._colors = colors;
		this._angle = random(0, 2 * Math.PI);
		this._t = t;
		this._t0 = t;
		this._coords = [{ x, y }];
		this.onAnimationFrameEnd(t => {
			var dt = rdt();
			while (this._t + dt < t && this._t - this._t0 <= this._duration) {
				this._t += dt;
				dt = rdt();
				var d = rdd();
				this._angle += rda();
				var { x, y } = last(this._coords);
				x += d * Math.cos(this._angle);
				y += d * Math.sin(this._angle);
				this._coords.push({ x, y });
			}
			//
			if (this._t - this._t0 > this._duration) {
				this._draw(this.background, lighter);
				this.remove();
			} else {
				this._draw(this.foreground);
			}
		});
	}
	_draw(context: Context, op: GlobalCompositeOperation = null) {
		var ctx = context.context;
		var widths = [5, 3, 1];
		var alphas = [.2, .2, .2];
		for (var i = 0; i < 3; i++) {
			var width = widths[i];
			var alpha = alphas[i];
			var color = this._colors[i];
			context.stroke(() => {
				var m = true;
				for (var { x, y } of this._coords) {
					if (m) context.moveTo(x, y);
					else context.lineTo(x, y);
					m = false;
				}
				ctx.lineJoin = "round";
				ctx.lineCap = "round";
				ctx.lineWidth = width;
				ctx.strokeStyle = color;
				ctx.globalAlpha = alpha;
				ctx.globalCompositeOperation = op;
			});
		}
	}
}

export default BurningLightning;
