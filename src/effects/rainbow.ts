import { Effect, Context, Gradient } from "../magic-painting.js";

const pow = Math.pow;

function setUpGradient(gradient: Gradient) {
	const pad = .1;
	// https://www.comfsm.fm/~dleeling/physci/text/112lab11.html
	const angles = [9, 17, 24, 24, 26, 30, 36, 43, 48, 52, 64, 77, 88, 111, 137, 165, 184, 200, 217, 230, 236, 239, 250, 270, 303];
	angles.reverse();
	const dOffset = (1 - 2 * pad) / (angles.length + 1);
	gradient.addColorStop(0, "white");
	gradient.addColorStop(pad, "white");
	angles.forEach((a, i) => gradient.addColorStop(pad + i * dOffset, `hsl(${a},100%,50%)`));
	gradient.addColorStop(1 - pad, "white");
	gradient.addColorStop(1, "white");
}

function pts2direction(p0: XY, p1: XY, normalize: boolean) {
	const { x: x0, y: y0 } = p0;
	const { x: x1, y: y1 } = p1;
	const dx = x1 - x0;
	const dy = y1 - y0;
	const d2 = dx * dx + dy * dy;
	if (d2 < 1) return null;
	var x = dx;
	var y = dy;
	if (normalize) {
		const d = Math.sqrt(d2);
		x /= d;
		y /= d;
	}
	return { x, y };
}

export class Rainbow extends Effect {
	xy: XY;
	tangent: XY;
	constructor() {
		super();
		//
		this.onPointerStart((x, y, t) => {
			this.xy = { x, y };
			this.tangent = undefined;
		});
		//
		this.onPointerEnd(t => {
			this.finish();
		});
		//
		this.onPointerMove((x, y, t) => {
			var { x: x0, y: y0 } = this.xy;
			if (!this.tangent) {
				let tan0 = pts2direction({ x: x0, y: y0 }, { x, y }, true);
				if (!tan0) return;
				this.tangent = tan0;
			}
			//
			const tan0 = this.tangent;
			const w = 40;
			const rMin = .55 * w;
			//
			const { x: ux0, y: uy0 } = tan0;
			const dxy = pts2direction({ x: x0, y: y0 }, { x, y }, false);
			if (!dxy) return;
			const { x: dx, y: dy } = dxy;
			const xLoc = + ux0 * dx + uy0 * dy;
			var yLoc = - uy0 * dx + ux0 * dy;
			const yLocSign = Math.sign(yLoc);
			var yLocAbs = Math.abs(yLoc);
			if (Math.abs(xLoc) >= rMin) var yLocMax = Number.POSITIVE_INFINITY;
			else var yLocMax = rMin - Math.sqrt(pow(rMin, 2) - pow(xLoc, 2));
			yLocAbs = Math.min(yLocAbs, yLocMax);
			yLoc = yLocSign * yLocAbs;
			x = x0 + ux0 * xLoc - uy0 * yLoc;
			y = y0 + uy0 * xLoc + ux0 * yLoc;
			const tan1 = pts2direction({ x: x0, y: y0 }, { x, y }, true);
			if (!tan1) return;
			const { x: ux, y: uy } = tan1;
			//
			const f = 1;
			x0 -= f * ux;
			y0 -= f * uy;
			//
			const a0 = Math.atan2(uy0, ux0);
			const a1 = Math.atan2(uy, ux);
			const a02 = a0 + .5 * Math.PI;
			const a12 = a1 + .5 * Math.PI;
			const cos0 = Math.cos(a02);
			const sin0 = Math.sin(a02);
			const cos1 = Math.cos(a12);
			const sin1 = Math.sin(a12);
			const x1 = x0 + w * cos0;
			const y1 = y0 + w * sin0;
			const x2 = x0 - w * cos0;
			const y2 = y0 - w * sin0;
			const x3 = x + w * cos1;
			const y3 = y + w * sin1;
			const x4 = x - w * cos1;
			const y4 = y - w * sin1;
			const xc1 = .5 * (x1 + x3);
			const yc1 = .5 * (y1 + y3);
			const xc2 = xc1 - cos1 * 2 * w;
			const yc2 = yc1 - sin1 * 2 * w;
			this.tangent = tan1;
			this.xy = { x, y };
			//
			const c = this.background;
			c.fill(() => {
				c.moveTo(x1, y1);
				c.lineTo(x2, y2);
				c.lineTo(x4, y4);
				c.lineTo(x3, y3);
				c.closePath();
				const gradient = c.createLinearGradient(xc1, yc1, xc2, yc2);
				setUpGradient(gradient);
				c.fillStyle = gradient;
			});
		});
	}
}
Rainbow.bePrimary();

export default Rainbow;
