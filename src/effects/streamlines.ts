import { Effect, Context } from "../magic-painting.js";

const white = "#ffffff";

export class StreamLines extends Effect {
	_xys: XY[] = [];
	constructor() {
		super();
		//
		this.onPointerStart((x, y, t) => {
			this._xys = [{ x, y }];
			var c = this.background;
			c.fill(() => {
				c.arc(x, y, 10, 0, 2 * Math.PI);
				c.context.fillStyle = white;
			});
			//
			this.add(new Circle());
			for (var i = 0; i < StreamLine.n; i++) this.add(new StreamLine(i));
		});
		//
		this.onPointerMove((x, y, t) => {
			this._xys.push({ x, y });
		});
		//
		this.onPointerEnd(t => {
			var c = this.background;
			this._drawLine(c);
			var { x, y } = this._xys.pop();
			for (var [r, a] of [[22, .1], [18, .1], [14, .1], [11, .1], [8, 1]]) {
				c.fill(() => {
					c.arc(x, y, r, 0, 2 * Math.PI);
					c.context.globalAlpha = a;
					c.context.fillStyle = white;
				});
			}
			//
			this._xys = [];
		});
		//
		this.onAnimationFrameEnd(aTime => {
			this._drawLine(this.foreground);
		});
		this.onRemoveChild(() => {
			if (this.numberOfChildren === 0) this.finish();
		});
	}
	private _drawLine(context: Context) {
		for (var [w, a] of [[30, .05], [25, .1], [20, .1], [15, .1], [10, .1], [6, .2], [3, 1]]) {
			context.stroke(() => {
				var m = true;
				for (var xy of this._xys) {
					var { x, y } = xy;
					if (m) context.moveTo(x, y);
					else context.lineTo(x, y);
					m = false;
				}
				var ctx = context.context;
				ctx.globalAlpha = a;
				ctx.lineCap = "round";
				ctx.lineJoin = "round";
				ctx.lineWidth = w;
				ctx.strokeStyle = white;
			});
		}
	}
}
StreamLines.bePrimary();

class Circle extends Effect {
	private _started = false;
	private _xy: XY;
	private _t: number;
	private _radii = new Array(20).fill(undefined).map((_, i) => 10 + 3 * i).reverse();
	private _alphas = new Array(20).fill(undefined).map((_, i) => .5 - .5 * i / 20).reverse();
	constructor() {
		super();
		this.onPointerStart((x, y, t) => {
			if (this._started) return;
			this._started = true;
			this._xy = { x, y };
			this._t = t;
		});
		this.onAnimationFrameStart(this._draw.bind(this));
	}
	private _draw(time: number) {
		var dt = 50;
		while (time - this._t > dt) {
			this._draw1();
			this._t += dt;
		}
		if (this._radii.length === 0) this.remove();
	}
	private _draw1() {
		var { x, y } = this._xy;
		var r = this._radii.pop();
		var a = this._alphas.pop();
		var c = this.background;
		c.stroke(() => {
			var ctx = c.context;
			c.arc(x, y, r, 0, 2 * Math.PI);
			ctx.globalAlpha = a;
			ctx.lineWidth = 3;
			ctx.strokeStyle = white;
		});
	}
}

class StreamLine extends Effect {
	static n = 64;
	private _pos = { x: 0, y: 0 };
	private _vel = { x: 0, y: 0 };
	private _vel0 = .15;
	private _target = { x: 0, y: 0 };
	private _t = 0;
	private _m = 1e5;
	private _f = (v: number) => {
		if (v > 100) return v;
		else return 10;
	};
	private _dt = 10;
	private _started = false;
	private _ended = false;
	private _alpha = 1;
	private _alphaRate = 2e-3;
	constructor(index: number) {
		super();
		var angle = index * 2 * Math.PI / StreamLine.n;
		var dAngle = 2;
		this._vel.x = this._vel0 * Math.cos(angle + dAngle);
		this._vel.y = this._vel0 * Math.sin(angle + dAngle);
		this.onPointerStart((x, y, t) => {
			if (this._started) return;
			this._started = true;
			this._target = { x, y };
			var r = 10;
			x += r * Math.cos(angle);
			y += r * Math.sin(angle);
			this._pos = { x, y };
			this._t = t;
		});
		this.onPointerMove((x, y, t) => {
			this._draw(t);
			if (!this._ended) this._target = { x, y };
		});
		this.onPointerEnd(t => {
			this._draw(t);
			this._ended = true;
		});
		this.onAnimationFrameEnd(aTime => {
			this._draw(aTime);
			if (this._alpha === 0) this.remove();
		});
	}
	private _draw(time: number) {
		var { x, y } = this._pos;
		var dt = this._dt;
		//
		var c = this.background;
		while (this._t + dt <= time && this._alpha !== 0) {
			c.stroke(() => {
				c.moveTo(x, y);
				var dx = this._target.x - x;
				var dy = this._target.y - y;
				var d2 = dx * dx + dy * dy;
				var d = Math.sqrt(d2);
				dx /= d;
				dy /= d;
				var f = this._f(d);
				var [fx, fy] = [dx, dy].map(v => v * f);
				var [ax, ay] = [fx, fy].map(v => v / this._m);
				this._vel.x += ax * dt;
				this._vel.y += ay * dt;
				x += this._vel.x * dt;
				y += this._vel.y * dt;
				c.lineTo(x, y);
				c.context.strokeStyle = white;
				if (this._ended) {
					this._alpha -= dt * this._alphaRate;
					this._alpha = Math.max(0, this._alpha);
				}
				c.context.globalAlpha = this._alpha;
			});
			this._t += dt;
		}
		this._pos = { x, y };
	}
}

export default StreamLines;
