type XYTCallback = (x?: number, y?: number, t?: number) => any;
type TCallback = (t?: number) => any;

type moveTo = ["moveTo", number, number];
type lineTo = ["lineTo", number, number];
type closePath = ["closePath"];
type arc = ["arc", number, number, number, number, number, boolean?];
type PathElement = moveTo | lineTo | arc | closePath;
type Path = PathElement[];

type Color = string;
type Style = Color | CanvasGradient | CanvasPattern | Gradient;

function index2transformation(i: number) {
	return 1 << i;
}

export const transformations = (function () {
	var [
		SYMMETRY_X,
		SYMMETRY_Y,
		ROTATION_30,
		ROTATION_45,
		ROTATION_60,
		ROTATION_90,
		ROTATION_120,
		ROTATION_135,
		ROTATION_150,
		ROTATION_180,
		ROTATION_210,
		ROTATION_225,
		ROTATION_240,
		ROTATION_270,
		ROTATION_300,
		ROTATION_315,
		ROTATION_330,
	] = new Array(17).fill(undefined).map((_, i) => index2transformation(i));
	return {
		SYMMETRY_X,
		SYMMETRY_Y,
		ROTATION_30,
		ROTATION_45,
		ROTATION_60,
		ROTATION_90,
		ROTATION_120,
		ROTATION_135,
		ROTATION_150,
		ROTATION_180,
		ROTATION_210,
		ROTATION_225,
		ROTATION_240,
		ROTATION_270,
		ROTATION_300,
		ROTATION_315,
		ROTATION_330,
	};
}());
const N_TRANSFORMATIONS = Object.keys(transformations).length;

/**
 * The MagicPainting class takes care of:
 * - **mouse/touch events**
 * - **window to canvas coordinate transformation**
 * - **schedules the painting** (using [**`requestAnimationFrame`**](https://developer.mozilla.org/docs/Web/API/window/requestAnimationFrame))
 * - **undo, redo, save, clear**
 * - **communication with the primary Effect** (settings, transformations, ...)
 *
 * Taking a HTMLElement, it creates two instances HTMLCanvasElement.
 * The foreground HTMLCanvasElement is cleared on every animation frame
 * and serves for temporary and changing painting / animations.
 * The background HTMLCanvasElement is meant for permanent drawings.
 *
 * The class provides a few on... methods. Others can be implemented
 */
export class MagicPainting {
	private static _instanceMouseActive: MagicPainting;
	private _element;
	private _cover;
	private _canvases;
	private _contexts;
	private _bcr: DOMRect;
	private _width;
	private _height;
	private _onPointerStartCallbacks: Function[] = [];
	private _onSaveCallbacks: Function[] = [];
	private _onAnimationFrameStartCallback: TCallback;
	private _effect: Effect;
	private _transformations: number;
	private _transformation2function;
	private _transformationFunctions: ((e: PathElement | Gradient) => PathElement | Gradient)[] = [];
	private _history;
	private _onHistoryChangeCallback: Function;
	/**
	 * @param element container element
	 * @param width canvas width (in canvas units)
	 * @param height canvas height (in canvas units)
	 */
	constructor(element: HTMLElement, width = 1000, height = 1000) {
		this._element = element;
		this._width = width;
		this._height = height;
		//
		var [background, foreground] = [0, 1].map(() => {
			var c = document.createElement("canvas");
			c.width = width;
			c.height = height;
			c.style.position = "absolute";
			c.style.width = "100%";
			c.style.height = "100%";
			this._element.appendChild(c);
			return c;
		});
		this._canvases = { background, foreground };
		this._contexts = {
			background: new Context(background, this),
			foreground: new Context(foreground, this),
		};
		this.clear();
		var cover = this._cover = document.createElement("div");
		this._element.appendChild(cover);
		cover.style.width = "100%";
		cover.style.height = "100%";
		cover.style.cursor = "pointer";
		cover.style.position = "absolute";
		cover.addEventListener("mousedown", this._onMouseDown.bind(this));
		window.addEventListener("mousemove", MagicPainting._onMouseMoveStatic);
		window.addEventListener("mouseup", MagicPainting._onMouseUpStatic);
		cover.addEventListener("touchstart", this._onTouchStart.bind(this), { passive: true });
		cover.addEventListener("touchmove", this._onTouchMove.bind(this), { passive: true });
		cover.addEventListener("touchend", this._onTouchEnd.bind(this));
		cover.addEventListener("touchcancel", this._onTouchCancel.bind(this));
		//
		new ResizeObserver(() => this._updateBCR()).observe(this._element);
		window.addEventListener("scroll", () => this._updateBCR());
		window.addEventListener("resize", () => this._updateBCR());
		this._updateBCR();
		//
		requestAnimationFrame(this._onAnimationFrameStart.bind(this));
		//
		this._transformation2function = {
			[transformations.SYMMETRY_X]: this._symmetry_x,
			[transformations.SYMMETRY_Y]: this._symmetry_y,
			[transformations.ROTATION_30]: this._rotate_30,
			[transformations.ROTATION_45]: this._rotate_45,
			[transformations.ROTATION_60]: this._rotate_60,
			[transformations.ROTATION_90]: this._rotate_90,
			[transformations.ROTATION_120]: this._rotate_120,
			[transformations.ROTATION_135]: this._rotate_135,
			[transformations.ROTATION_150]: this._rotate_150,
			[transformations.ROTATION_180]: this._rotate_180,
			[transformations.ROTATION_210]: this._rotate_210,
			[transformations.ROTATION_225]: this._rotate_225,
			[transformations.ROTATION_240]: this._rotate_240,
			[transformations.ROTATION_270]: this._rotate_270,
			[transformations.ROTATION_300]: this._rotate_300,
			[transformations.ROTATION_315]: this._rotate_315,
			[transformations.ROTATION_330]: this._rotate_330,
		};
		//
		this.transformations = 0;
		this._history = new History(this.imageData);
	}
	/**
	 * Underlying canvas elements
	 */
	get canvases() {
		return { ...this._canvases };
	}
	/**
	 * Rendering contexts of underlying canvas elements
	 */
	get contexts() {
		return { ...this._contexts };
	}
	/**
	 * Primary Effect
	 */
	get effect() {
		return this._effect;
	}
	set effect(effect) {
		if (!(effect instanceof Effect)) throw new Error("MagicPainting effect has to be an Effect instance");
		if (!effect.isPrimary()) throw new Error("MagicPainting effect has to be a primary effect");
		this._effect = effect;
		// @ts-expect-error: private
		effect._magicPainting = this;
	}
	/**
	 * Applied transformations
	 */
	get transformations() {
		return this._transformations;
	}
	set transformations(v) {
		this._transformations = v;
		this._transformationFunctions = this._getTransformationFunctions();
	}
	/**
	 * Settings of the primary Effect
	 */
	get settings() {
		return this._effect?.settings ?? {};
	}
	set settings(v) {
		if (!this._effect) return;
		this._effect.settings = { ...this.settings, ...v };
	}
	submitSettings() {
		this._effect?.submitSettings();
	}
	//
	get isUndoable() {
		return this._history.isUndoable;
	}
	get isRedoable() {
		return this._history.isRedoable;
	}
	/**
	 * ImageData of the background canvas
	 */
	get imageData() {
		return this._contexts.background.context.getImageData(0, 0, this._width, this._height);
	}
	/**
	 * Save backround canvas image data to a .png file
	 */
	save() {
		var e = this._canvases.background;
		var link = document.createElement("a");
		link.setAttribute("download", "magic-painting.png");
		link.setAttribute("href", e.toDataURL("image/png"));
		link.click();
		this._onSaveCallbacks.forEach(callback => callback());
		return this;
	}
	/**
	 * Clear background canvas
	 */
	clear() {
		var ctx = this.contexts.background.context;
		ctx.save();
		ctx.fillStyle = "#000";
		ctx.fillRect(0, 0, this._width, this._height);
		ctx.restore();
		this._history?.reset();
		this._onHistoryChangeCallback?.();
		return this;
	}
	/**
	 * Freeze itself (disabling user interaction).
	 * Useful e.g. for scripted only painting.
	 */
	freeze() {
		this._cover.style.visibility = "hidden";
		return this;
	}
	/**
	 * What to do on save.
	 * App uses this event for beforeunload window event (warning about unsaved changes)
	 */
	onSave(callback: Function) {
		this._onSaveCallbacks.push(callback);
		return this;
	}
	/**
	 * What to do on pointer start.
	 * App uses this event for beforeunload window event (warning about unsaved changes)
	 */
	onPointerStart(callback: Function) {
		this._onPointerStartCallbacks.push(callback);
		return this;
	}
	//
	/**
	 * Undo last change
	 */
	undo() {
		if (!this.isUndoable) return this;
		this._history.undo();
		return this._updateFromHistory();
	}
	/**
	 * Redo lastly undone  change
	 */
	redo() {
		if (!this.isRedoable) return this;
		this._history.redo();
		return this._updateFromHistory();
	}
	/**
	 * What to do on history change.
	 * App uses this callback to make undo/redo buttons up-to-date
	 */
	onHistoryChange(callback: Function) {
		this._onHistoryChangeCallback = callback;
		return this;
	}
	//
	private _onMouseDown(e: MouseEvent) {
		MagicPainting._instanceMouseActive = this;
		var { clientX, clientY } = e;
		var { x, y } = this._client2canvas(clientX, clientY);
		this._onPointerStart(x, y, e.timeStamp);
	}
	private _onMouseMove(e: MouseEvent) {
		var { clientX, clientY } = e;
		var { x, y } = this._client2canvas(clientX, clientY);
		this._onPointerMove(x, y, e.timeStamp);
	}
	private _onMouseUp(e: MouseEvent) {
		this._onPointerEnd(e.timeStamp);
	}
	private _onTouchStart(e: TouchEvent) {
		var { clientX, clientY } = e.touches[0];
		var { x, y } = this._client2canvas(clientX, clientY);
		this._onPointerStart(x, y, e.timeStamp);
	}
	private _onTouchMove(e: TouchEvent) {
		var { clientX, clientY } = e.touches[0];
		var { x, y } = this._client2canvas(clientX, clientY);
		this._onPointerMove(x, y, e.timeStamp);
	}
	private _onTouchEnd(e: TouchEvent) {
		e.preventDefault();
		this._onPointerEnd(e.timeStamp);
	}
	private _onTouchCancel(e: TouchEvent) {
		e.preventDefault();
		this._onPointerEnd(e.timeStamp);
	}
	private static _onMouseMoveStatic(e: MouseEvent) {
		if (!MagicPainting._instanceMouseActive) return;
		MagicPainting._instanceMouseActive._onMouseMove(e);
	}
	private static _onMouseUpStatic(e: MouseEvent) {
		if (!MagicPainting._instanceMouseActive) return;
		MagicPainting._instanceMouseActive._onMouseUp(e);
		MagicPainting._instanceMouseActive = null;
	}
	private _onPointerStart(x: number, y: number, t: number) {
		this._onPointerStartCallbacks.forEach(callback => callback(x, y, t));
		this._effect?.pointerStart(x, y, t);
	}
	private _onPointerMove(x: number, y: number, t: number) {
		this._effect?.pointerMove(x, y, t);
	}
	private _onPointerEnd(t: number) {
		this._effect?.pointerEnd(t);
	}
	//
	private _client2canvas(clientX: number, clientY: number) {
		var bcr = this._bcr;
		var x = clientX - bcr.x;
		var y = clientY - bcr.y;
		x *= this._width / bcr.width;
		y *= this._height / bcr.height;
		return { x, y };
	}
	private _updateBCR() {
		this._bcr = this._cover.getBoundingClientRect();
	}
	private _onAnimationFrameStart(aTime: number) {
		this._contexts.foreground.context.clearRect(0, 0, this._width, this._height);
		this._onAnimationFrameStartCallback?.(aTime);
		requestAnimationFrame(this._onAnimationFrameStart.bind(this));
	}
	//
	/**
	 * Returns transformed copies of given path w.r.t this.transformations
	 */
	getPaths(path: Path) {
		var ret = [path];
		for (var trsf of this._transformationFunctions) {
			var p = path.map(e => trsf.call(this, e) as PathElement);
			ret.push(p);
		}
		return ret;
	}
	/**
	 * Returns transformed copies of given style w.r.t this.transformations
	 */
	getStyles(style: Style, context: CanvasRenderingContext2D) {
		var ret = [] as Style[];
		var trsfs = [(e: any) => e, ...this._transformationFunctions];
		for (var trsf of trsfs) {
			var s: Style;
			if (style instanceof Gradient) {
				var g = trsf.call(this, style) as Gradient;
				s = g.toStyle(context);
			} else {
				s = style;
			}
			ret.push(s);
		}
		return ret;
	}
	/**
	 * Returns transformed gradients of given path w.r.t this.transformations
	 */
	getGradients(gradient: Gradient) {
		var ret = [gradient];
		for (var trsf of this._transformationFunctions) {
			var g = trsf.call(this, gradient) as Gradient;
			ret.push(g);
		}
		return ret;
	}
	private _getTransformationFunctions() {
		var ret = [];
		for (var i = 0; i < N_TRANSFORMATIONS; i++) {
			var t = index2transformation(i);
			if (this._transformations & t) ret.push(this._transformation2function[t]);
		}
		return ret;
	}
	private _symmetry_x(e: PathElement | Gradient): PathElement | Gradient {
		var h = this._height;
		if (e instanceof Gradient) {
			e = e.clone();
			if (e instanceof LinearGradient) {
				e.y0 = h - e.y0;
				e.y1 = h - e.y1;
			}
			return e;
		}
		var [k, v1, v2, v3, v4, v5, v6] = e;
		if (k === "moveTo" || k === "lineTo") return [k, v1, h - v2];
		if (k === "arc") return [k, v1, h - v2, v3, v4, v5, v6]; // TODO
		if (k === "closePath") return ["closePath"];
	}
	private _symmetry_y(e: PathElement | Gradient): PathElement | Gradient {
		var w = this._width;
		if (e instanceof Gradient) {
			e = e.clone();
			if (e instanceof LinearGradient) {
				e.x0 = w - e.x0;
				e.x1 = w - e.x1;
			}
			return e;
		}
		var [k, v1, v2, v3, v4, v5, v6] = e;
		if (k === "moveTo" || k === "lineTo") return [k, w - v1, v2];
		if (k === "arc") return [k, w - v1, v2, v3, v4, v5, v6]; // TODO
		if (k === "closePath") return ["closePath"];
	}
	private _rotate(e: PathElement | Gradient, a: number): PathElement | Gradient {
		a *= Math.PI / 180;
		var w = this._width, h = this._height;
		var w2 = .5 * w, h2 = .5 * h;
		const rot = (x: number, y: number) => {
			let dx = x - w2, dy = y - h2;
			let r = Math.sqrt(dx * dx + dy * dy);
			let aa = Math.atan2(dy, dx);
			aa += a;
			x = w2 + r * Math.cos(aa);
			y = h2 + r * Math.sin(aa);
			return { x, y };
		};
		//
		if (e instanceof Gradient) {
			e = e.clone();
			if (e instanceof LinearGradient) {
				const xy0 = rot(e.x0, e.y0);
				const xy1 = rot(e.x1, e.y1);
				e.x0 = xy0.x;
				e.y0 = xy0.y;
				e.x1 = xy1.x;
				e.y1 = xy1.y;
			}
			return e;
		}
		//
		var [k, v1, v2, v3, v4, v5, v6] = e;
		if (k === "moveTo" || k === "lineTo") {
			let { x, y } = rot(v1, v2);
			return [k, x, y];
		}
		if (k === "arc") {
			let { x, y } = rot(v1, v2);
			return [k, x, y, v3, v4, v5, v6]; // TODO
		}
		if (k === "closePath") return ["closePath"];
	}
	private _rotate_30(e: PathElement) {
		return this._rotate(e, 30);
	}
	private _rotate_45(e: PathElement) {
		return this._rotate(e, 45);
	}
	private _rotate_60(e: PathElement) {
		return this._rotate(e, 60);
	}
	private _rotate_90(e: PathElement) {
		return this._rotate(e, 90);
	}
	private _rotate_120(e: PathElement) {
		return this._rotate(e, 120);
	}
	private _rotate_135(e: PathElement) {
		return this._rotate(e, 135);
	}
	private _rotate_150(e: PathElement) {
		return this._rotate(e, 150);
	}
	private _rotate_180(e: PathElement) {
		return this._rotate(e, 180);
	}
	private _rotate_210(e: PathElement) {
		return this._rotate(e, 210);
	}
	private _rotate_225(e: PathElement) {
		return this._rotate(e, 225);
	}
	private _rotate_240(e: PathElement) {
		return this._rotate(e, 240);
	}
	private _rotate_270(e: PathElement) {
		return this._rotate(e, 270);
	}
	private _rotate_300(e: PathElement) {
		return this._rotate(e, 300);
	}
	private _rotate_315(e: PathElement) {
		return this._rotate(e, 315);
	}
	private _rotate_330(e: PathElement) {
		return this._rotate(e, 330);
	}
	private _onFinishEffect() {
		setTimeout(() => {
			this._history.push(this.imageData);
			this._onHistoryChangeCallback?.();
		});
	}
	private _updateFromHistory() {
		var data = this._history.last();
		this._contexts.background.context.putImageData(data, 0, 0);
	}
}

class History {
	private _done: ImageData[];
	private _undone: ImageData[];
	constructor(data0: ImageData) {
		this._done = [data0];
		this._undone = [];
	}
	get isUndoable() {
		return this._done.length > 1;
	}
	get isRedoable() {
		return this._undone.length > 0;
	}
	undo() {
		var data = this._done.pop();
		this._undone.push(data);
		return this.last();
	}
	redo() {
		var data = this._undone.pop();
		this._done.push(data);
		return this.last();
	}
	last() {
		return this._done[this._done.length - 1];
	}
	push(data: ImageData) {
		this._done.push(data);
		this._undone = [];
		return this;
	}
	reset() {
		this._done.length = 1;
		this._undone.length = 0;
		return this;
	}
}


export class Gradient {
	protected _offsets: number[] = [];
	protected _colors: Color[] = [];
	addColorStop(offset: number, color: Color) {
		this._offsets.push(offset);
		this._colors.push(color);
	}
	toStyle(context: CanvasRenderingContext2D): CanvasGradient {
		return null;
	}
	protected _applyColorStops(gradient: CanvasGradient) {
		const n = this._offsets.length;
		for (var i = 0; i < n; i++) {
			const offset = this._offsets[i];
			const color = this._colors[i];
			gradient.addColorStop(offset, color);
		}
	}
	clone() {
		const ret = this._clone();
		ret._offsets = this._offsets.slice();
		ret._colors = this._colors.slice();
		return ret;
	}
	protected _clone(): Gradient {
		return null;
	}
}

export class LinearGradient extends Gradient {
	x0: number;
	y0: number;
	x1: number;
	y1: number;
	constructor(x0: number, y0: number, x1: number, y1: number) {
		super();
		this.x0 = x0;
		this.y0 = y0;
		this.x1 = x1;
		this.y1 = y1;
	}
	toStyle(context: CanvasRenderingContext2D) {
		const { x0, y0, x1, y1 } = this;
		var ret = context.createLinearGradient(x0, y0, x1, y1);
		this._applyColorStops(ret);
		return ret;
	}
	protected _clone() {
		const { x0, y0, x1, y1 } = this;
		return new LinearGradient(x0, y0, x1, y1);
	}
}

/**
 * The Context class is a wrapper around [`CanvasRenderingContext2D`](https://developer.mozilla.org/docs/Web/API/CanvasRenderingContext2D).
 * Its main purpose is to **automatically apply transformations (symmetry, rotation etc.)**, so if something is painted at one place, it is simultanously painted on other relevant places "out of the box", too.
 * Currently implemented are:
 * - symmetry along x axis
 * - symmetry along y axis
 * - symmetry along x axis, y axis and center
 */
export class Context {
	private _context;
	private _magicPainting;
	private _path: Path;
	private _fillStyle: Style;
	private _strokeStyle: Style;
	constructor(canvas: HTMLCanvasElement, magicPainting: MagicPainting) {
		this._context = canvas.getContext("2d");
		this._magicPainting = magicPainting;
	}
	/**
	 * Underlying context
	 */
	get context() {
		return this._context;
	}
	//
	moveTo(x: number, y: number) {
		this._path.push(["moveTo", x, y]);
		return this;
	}
	lineTo(x: number, y: number) {
		this._path.push(["lineTo", x, y]);
		return this;
	}
	arc(x: number, y: number, radius: number, startAngle: number, endAngle: number, counterclockwise?: boolean) {
		this._path.push(["arc", x, y, radius, startAngle, endAngle, counterclockwise]);
		return this;
	}
	createLinearGradient(x1: number, y1: number, x2: number, y2: number) {
		return new LinearGradient(x1, y1, x2, y2);
	}
	closePath() {
		this._path.push(["closePath"]);
		return this;
	}
	//
	/**
	 * What to do before/around fill.
	 * Calls:
	 * - this.context.save
	 * - callback
	 * - this.context.beginPath
	 * - apply drawing stored via this.moveTo, this.lineTo etc.
	 * - this.context.fill
	 * - this.context.restore
	 */
	fill(callback: Function) {
		return this._strokeOrFill(callback, true);
	}
	/**
	 * What to do before/around stroke.
	 * Calls:
	 * - this.context.save
	 * - callback
	 * - this.context.beginPath
	 * - apply drawing stored via this.moveTo, this.lineTo etc.
	 * - this.context.stroke
	 * - this.context.restore
	 */
	stroke(callback: Function) {
		return this._strokeOrFill(callback, false);
	}
	set strokeStyle(style: Style) {
		this._strokeStyle = style;
	}
	set fillStyle(style: Style) {
		this._fillStyle = style;
	}
	private _strokeOrFill(callback: Function, fill = false) {
		this._path = [];
		this._fillStyle = null;
		this._strokeStyle = null;
		var ctx = this._context;
		ctx.save();
		callback();
		var paths = this._magicPainting.getPaths(this._path);
		var strokeStyles = this._magicPainting.getStyles(this._strokeStyle, ctx);
		var fillStyles = this._magicPainting.getStyles(this._fillStyle, ctx);
		const n = paths.length;
		for (var i = 0; i < n; i++) {
			const path = paths[i];
			const strokeStyle = strokeStyles[i];
			const fillStyle = fillStyles[i];
			ctx.beginPath();
			for (var e of path) {
				var [k, v1, v2, v3, v4, v5, v6] = e;
				if (k === "moveTo") ctx.moveTo(v1, v2);
				else if (k === "lineTo") ctx.lineTo(v1, v2);
				else if (k === "arc") ctx.arc(v1, v2, v3, v4, v5, v6);
				else if (k === "closePath") ctx.closePath();
			}
			//
			if (strokeStyle) ctx.strokeStyle = strokeStyle;
			if (fillStyle) ctx.fillStyle = fillStyle;
			//
			if (fill) ctx.fill();
			else ctx.stroke();
		}
		ctx.restore();
		return this;
	}
}

/**
 * The `Effect` class is the base class for actual painting.
 * Its subclasses implements what to do based on given events (i.e. their coordinates and times)
 * Each effect can have a parent effect and descendant child effect(s).
 *
 * The core of the sub-classes is providing
 * onPointerStart,
 * onPointerMove,
 * onPointerEnd
 * and
 * onAnimationFrameEnd callbacks.
 *
 * The callback is first evaluated on the receiver and then passed to its children (with same arguments).
 */
export class Effect {
	private _onPointerStartCallback: XYTCallback;
	private _onPointerMoveCallback: XYTCallback;
	private _onPointerEndCallback: TCallback;
	private _onAnimationFrameStartCallback: Function;
	private _onAnimationFrameEndCallback: Function;
	private _onRemoveChildCallback: Function;
	private _children = new Set<Effect>();
	private _parent: Effect;
	private _requestAnimationFrameEndId: number;
	private _magicPainting: MagicPainting;
	settings: { [x: string]: any; } = {};
	//
	constructor(isPrimary?: boolean) {
		if (isPrimary === true) this.isPrimary = () => true;
		if (isPrimary === false) this.isPrimary = () => false;
		if (!this.isPrimary()) return;
		//
		requestAnimationFrame(this._animationFrameStart.bind(this));
		this._reRequestAnimationFrameEnd();
	}
	/**
	 * Children effects
	 */
	get children() {
		return new Set(this._children);
	}
	/**
	 * Number of children effects
	 */
	get numberOfChildren() {
		return this._children.size;
	}
	/**
	 * Parent effect
	 */
	get parent() {
		return this._parent;
	}
	/**
	 * background Context
	 */
	get background() {
		return this._magicPainting.contexts.background;
	}
	/**
	 * foreground Context
	 */
	get foreground() {
		return this._magicPainting.contexts.foreground;
	}
	isPrimary() {
		return false;
	}
	/**
	 * Makes the class a primary effect
	 */
	static bePrimary() {
		this.prototype.isPrimary = () => true;
	}
	/**
	 * Add effect
	 */
	add(child: Effect) {
		this._children.add(child);
		child._parent = this;
		child._magicPainting = this._magicPainting;
		return this;
	}
	/**
	 * Remove this from parent effect
	 */
	remove() {
		var p = this._parent;
		if (!p) return this;
		p._children.delete(this);
		p._onRemoveChildCallback?.();
		return this;
	}
	/**
	 * What to do if a child is removed.
	 * It can be used to check when to finish itself.
	 */
	onRemoveChild(callback: Function) {
		this._onRemoveChildCallback = callback;
		return this;
	}
	/**
	 * Finish itself.
	 * App uses this information for undo/redo frames.
	 */
	finish() {
		// @ts-expect-error: private
		this._magicPainting._onFinishEffect();
		return this;
	}
	//
	//
	/**
	 * What to do on pointer start.
	 * @param callback function acception (x coordinate, y coordinate, t event time) arguments
	 */
	onPointerStart(callback: XYTCallback) {
		this._onPointerStartCallback = callback;
		return this;
	}
	/**
	 * What to do on pointer move.
	 * @param callback function acception (x coordinate, y coordinate, t event time) arguments
	 */
	onPointerMove(callback: XYTCallback) {
		this._onPointerMoveCallback = callback;
		return this;
	}
	/**
	 * What to do on pointer end
	 * @param callback function acception (t event time) argument
	 */
	onPointerEnd(callback: TCallback) {
		this._onPointerEndCallback = callback;
		return this;
	}
	/**
	 * What to do on animation frame start
	 * @param callback function acception (t animation frame time) argument
	 */
	onAnimationFrameStart(callback: TCallback) {
		this._onAnimationFrameStartCallback = callback;
		return this;
	}
	/**
	 * What to do on animation frame end
	 * @param callback function acception (t animation frame time) argument
	 */
	onAnimationFrameEnd(callback: TCallback) {
		this._onAnimationFrameEndCallback = callback;
		return this;
	}
	//
	/**
	 * Start the effect, providing x,y coordinates and time
	 */
	pointerStart(x: number, y: number, t: number) {
		requestAnimationFrame(aTime => this._pointerStart?.(x, y, Math.min(t, aTime)));
		this._reRequestAnimationFrameEnd();
		return this;
	}
	/**
	 * Move the effect, providing x,y coordinates and time
	 */
	pointerMove(x: number, y: number, t: number) {
		requestAnimationFrame(aTime => this._pointerMove?.(x, y, Math.min(t, aTime)));
		this._reRequestAnimationFrameEnd();
		return this;
	}
	/**
	 * End the effect, providing time
	 */
	pointerEnd(t: number) {
		requestAnimationFrame(aTime => this._pointerEnd?.(Math.min(t, aTime)));
		this._reRequestAnimationFrameEnd();
		return this;
	}
	//
	submitSettings() {
	}
	//
	private _animationFrameStart(aTime: number) {
		this._onAnimationFrameStart(aTime);
		requestAnimationFrame(this._animationFrameStart.bind(this));
	}
	private _onAnimationFrameStart(aTime: number) {
		this._onAnimationFrameStartCallback?.(aTime);
		this._children.forEach(child => child._onAnimationFrameStart(aTime));
	}
	private _animationFrameEnd(aTime: number) {
		this._onAnimationFrameEnd(aTime);
		this._reRequestAnimationFrameEnd();
	}
	private _onAnimationFrameEnd(aTime: number) {
		this._onAnimationFrameEndCallback?.(aTime);
		this._children.forEach(child => child._onAnimationFrameEnd(aTime));
	}
	private _reRequestAnimationFrameEnd() {
		cancelAnimationFrame(this._requestAnimationFrameEndId);
		this._requestAnimationFrameEndId = requestAnimationFrame(this._animationFrameEnd.bind(this));
	}
	private _pointerStart(x: number, y: number, t: number) {
		this._onPointerStartCallback?.(x, y, t);
		this._children.forEach(child => child._pointerStart(x, y, t));
	}
	private _pointerMove(x: number, y: number, t: number) {
		this._onPointerMoveCallback?.(x, y, t);
		this._children.forEach(child => child._pointerMove(x, y, t));
	}
	private _pointerEnd(t: number) {
		this._onPointerEndCallback?.(t);
		this._children.forEach(child => child._pointerEnd(t));
	}
}

export default MagicPainting;
