window.addEventListener("load", () => {
	for (var [name, Effect] of Object.entries(MagicPainting.effects)) {
		var container = document.createElement("div");
		var title = document.createElement("div");
		title.innerText = name;
		var painting = document.createElement("div");
		painting.style.position = "relative";
		painting.style.width = "400px";
		painting.style.height = "400px";
		document.body.appendChild(container);
		container.appendChild(title);
		container.appendChild(painting);
		var magicPainting = new MagicPainting.MagicPainting(painting);
		magicPainting.effect = new Effect();
	}
});
