# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] - yyyy-mm-dd

## [0.1.1] - 2022-03-17
### Changed
- app - improved touch on mobile devices
### Added
- linear gradient (and general gradient concept)
- rainbow effect
- rotation transformations

## [0.1.0] - 2021-10-25
### Added
- auto-version scripts

## [0.0.0] - 2021-10-25
### Added
- classes:
  - MagicPainting
  - Context
  - Effect
- effects:
  - BurningLightning
  - StreamLines
- first version of app
