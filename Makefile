all: page

pages:
	gitlab-runner exec shell pages

clean:
	rm -rf builds/
	rm -rf public/
